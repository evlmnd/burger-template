import React from 'react';
import './BuildControls.css';
import BuildControl from "./BuildControl/BuildControl";

const controls = [
    {type: 'salad'},
    {type: 'bacon'},
    {type: 'cheese'},
    {type: 'meat'}
];

const BuildControls = props => {

    return (
        <div className="BuildControls">
            <p>Current price: <strong>{props.price}</strong></p>
            {controls.map(control => (
                <BuildControl
                    key={control.type}
                    type={control.type}
                    added={() => props.add(control.type)}
                    removed={() => props.remove(control.type)}
                    disabled={props.disabled[control.type]}

                />
            ))}
            <button
                className="OrderButton"
                disabled={!props.purchasable}
                onClick={props.purchase}
            >
                ORDER NOW
            </button>
        </div>
    )

};

export default BuildControls;