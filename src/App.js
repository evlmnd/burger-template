import React, { Component } from 'react';
import BurgerBuilder from "./containers/BurgerBuilder/BurgerBuilder";
import CheckOut from './containers/Checkout/Checkout';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import Orders from "./containers/Orders/Orders";


class App extends Component {



  render() {
    return (
        <Layout>
          <Switch>
            <Route path="/" exact component={BurgerBuilder}/>
            <Route path="/checkout" component={CheckOut}/>
            <Route path="/orders" component={Orders}/>
          </Switch>
        </Layout>
    );
  }
}

export default App;
